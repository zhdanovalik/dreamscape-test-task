'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');

var autoprefixer = require('gulp-autoprefixer');
var del = require('del');

var gutil = require('gulp-util');
var newer = require('gulp-newer');
var remember = require('gulp-remember');
var cache = require('gulp-cached');
var browserSync = require('browser-sync').create();
var reload =  browserSync.reload;
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");

var paths = {
    img: {
        src: 'app/img/for_sprite/',
        dest: 'app/img/sprite/sprite.svg'
    },
    scripts: {
        src: 'static/coffee/',
        dest: 'static/js/'
    },
    styles: {
        src: 'app/sass/',
        dest: 'app/css/'
    },
};

var appFiles = {
    styles: paths.styles.src + 'style.scss',
    stylesWatch: paths.styles.src + '**/*.scss',
    scripts: paths.scripts.src + 'js.coffee',
    scriptsDest: paths.scripts.src + 'js.js',
    images: paths.img.src + '*.svg'
};


gulp.task('styles', function() {
    return gulp.src(appFiles.styles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({browsers: ['> 1%', 'IE 9']}))
        .pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream())
        .pipe(notify("Compiled"));
});



gulp.task('watch',['styles'], function(){

    browserSync.init({
        server: "./app"
    });

    gulp.watch('app/sass/**/*', ['styles']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
		gulp.watch("app/js/*.js").on('change', browserSync.reload);

});

gulp.task('build',['scripts', 'styles'], function() {
    console.log('***build finished correct***');
});
gulp.task('default',['scripts', 'styles'], function() {
    console.log('***build finished correct***');
});
