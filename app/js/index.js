/*jslint node: true */
/* jshint browser: true */
"use strict";

function scrollToElement (to, duration) {
	var html = document.documentElement;
	var body = document.body;
	var scrollTop = html.scrollTop || body && body.scrollTop || 0;

	var difference = to.offsetTop - scrollTop;

	var perTick = difference / duration * 10;

	setTimeout(function() {
		window.scrollTo(0,scrollTop +10);
		if (scrollTop >= to.offsetTop) return;
	 	scrollToElement (to, duration-10);
  }, 10);
}

document.querySelector('.js-header-arrow').onclick = function(e) {
	e.preventDefault();
	var elem = document.getElementById('main');
	scrollToElement(elem, 20);
};

//////////////////////////////////////////////////////
// slider
//////////////////////////////////////////////////////

//define buttons
var prevButton = document.querySelector('.slider-prev');
var nextButton = document.querySelector('.slider-next');
//define slider
var slider = document.querySelector('.slider');
var sliderItems = slider.querySelectorAll('.slider-item');

function changeClass (parent, number, add, remove) {
	var item = parent[number].classList;
	item.add(add);
	if (item.contains(remove) !== null) {
		item.remove(remove);
	}
	if (item.contains(remove + '-rev') !== null) {
		item.remove(remove + '-rev');
	}
}

function sliderNext () {
	for (var i = 0; i < sliderItems.length; i++) {
		if (sliderItems[i].classList.contains('active') || sliderItems[i].classList.contains('active-rev')) {
			changeClass (sliderItems, i, 'hidden', 'active');
			if ((i+1) < sliderItems.length) {
				changeClass (sliderItems, (i+1), 'active', 'shown');

				if ((i+2)< sliderItems.length) {
					changeClass (sliderItems, (i+2), 'shown', 'hidden');
				} else {
					changeClass (sliderItems, 0, 'shown', 'hidden');
				}
			} else {
				changeClass (sliderItems, 0, 'active', 'shown');
				changeClass (sliderItems, 1, 'shown', 'hidden');
			}
			break;
		}
	}
}

function sliderPrev () {
	for (var i = 0; i < sliderItems.length; i++) {
		if (sliderItems[i].classList.contains('active') || sliderItems[i].classList.contains('active-rev')) {
			changeClass (sliderItems, i, 'shown-rev', 'active');

			if ((i-1) >= 0) {
				changeClass (sliderItems, (i-1), 'active-rev', 'hidden');
			} else {
				changeClass (sliderItems, (sliderItems.length - 1), 'active-rev', 'hidden');
			}
			if ((i+1)< sliderItems.length) {
				changeClass (sliderItems, (i+1), 'hidden', 'shown');
			} else {
				changeClass (sliderItems, 0, 'hidden', 'shown');
			}
			break;
		}
	}

}

// jquery index() analog
function indexInParent(node) {
    var children = node.parentNode.childNodes;
    var num = 0;
    for (var i=0; i<children.length; i++) {
         if (children[i]==node) return num;
         if (children[i].nodeType==1) num++;
    }
    return -1;
}
